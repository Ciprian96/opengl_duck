#include<GL/freeglut.h>
#include<stdlib.h>

#include<stdio.h>
#include<math.h>

static float angle = 0.0f;
static float zoom = -25.0f;
static float bow = 60.0f;
static float lean = 0.0f;
int k = 0;
int l = 0;

void initGL(int width,int height)
{
    const GLfloat light_ambient[]={0.0f,0.0f,0.0f,1.0f};
    const GLfloat light_diffuse[]={1.0f,1.0f,1.0f,1.0f};
    const GLfloat light_specular[]={1.0f,1.0f,1.0f,1.0f};
    const GLfloat light_position[]={2.0f,5.0f,5.0f,0.0f};

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glLightfv(GL_LIGHT0,GL_AMBIENT,light_ambient);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,light_diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,light_specular);
    glLightfv(GL_LIGHT0,GL_POSITION,light_position);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_COLOR_MATERIAL);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)width/(GLfloat)height,2.0f,100.0f);
    glMatrixMode(GL_MODELVIEW);
}
static void
display(void)
{
    static int frame,timebase=0;
    int time;
    char s[100];
    frame++;
    time=glutGet(GLUT_ELAPSED_TIME);
    if (time-timebase>1000)
    {
        sprintf(s,"[FPS:%4.2f] Duck:",frame*1000.0/(time-timebase));
        glutSetWindowTitle(s);
        timebase=time;
        frame=0;
    }
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();//initializarea sistemului de coordonate

    static float x, y, z =0.0f;
    static float axisRot = 0.0f;





    glPushMatrix(); //corp
    glColor3f(1.0f, 1.0f, 0.0f);
    glTranslatef(0.5f,-4.0f,zoom);
    glRotatef(angle,0,1,0);
    glScalef(1.2,0.9,1.5);
    glutSolidSphere(3.4,16,10);
//////////////////////////////////////////
///CAP
//////////////////////////////////////////

    glPushMatrix();//gat
    glColor3f(1.0f,1.0f,0.0f);
    glTranslatef(0.0f,1.2f,2.0f);
    glRotatef(-bow,1,0,0);
    glRotatef(lean,0,1,0);
    glScalef(1.0,1,0.9);
    glutSolidCylinder(1.1,2,16,10);


    glPushMatrix();//cap
    glColor3f(1.0f,1.0f,0.0f);
    glTranslatef(0.0f,0.1f,2.5f);
    glRotatef(60,1,0,0);
    glScalef(1.0,1,0.9);
    glutSolidSphere(2.0,16,10);

    glPushMatrix();//pata stg
    glColor3f(1.0f,1.0f,1.0f);
    glTranslatef(-0.7f,0.2f,1.5f);
    glScalef(1,1.3,0.7);
    glutSolidSphere(0.6,20,10);
    glPopMatrix();

    glPushMatrix();//pata dr
    glColor3f(1.0f,1.0f,1.0f);
    glTranslatef(0.7f,0.2f,1.5f);
    glScalef(1,1.3,0.7);
    glutSolidSphere(0.6,20,10);
    glPopMatrix();



    glPushMatrix();//gurita
    glColor3f(1.00, 0.5, 0.0);
    glTranslated(0.0f, -0.5f,1.9f);
    glRotatef(-80,1,0,0);
    //glRotatef(295,0,0,0);
    glScalef(2,4,0.8);
    glutSolidSphere(0.2, 6, 10);
    glPopMatrix();

    glPushMatrix();//limba
    glColor3f(0.5, 0.0, 0.0);
    glTranslated(0.0f, -0.65f,1.9f);
    glRotatef(-80,1,0,0);
    //glRotatef(295,0,0,0);
    glScalef(1,3,0.5);
    glutSolidSphere(0.2, 20, 10);
    glPopMatrix();

    glPushMatrix();//gurita
    glColor3f(1.00, 0.5, 0.0);
    glTranslated(0.0f, -0.7f,1.9f);
    glRotatef(-70,20,0,0);
    glRotatef(x,1,0,0);
    glScalef(2,4,0.7);
    glutSolidSphere(0.2, 6, 10);
    glPopMatrix();


    glPushMatrix();//ochi stang
    glColor3f(0.419608, 0.556863, 0.137255);
    glTranslatef(-0.8f,0.4f,1.2f);
    //glScalef(0.3,0.3,0.3);
    glutSolidSphere(0.5,20,10);
    glPushMatrix();//pupila stang
    glColor3f(0.0f,0.0f,0.0f);
    glTranslatef(0.0f,0.0f,0.5f);
    //glScalef(0.1,0.1,0.1);
    glutSolidSphere(0.3,20,10);
    glPopMatrix();
    glPopMatrix();


    glPushMatrix();//ochi drept
    glColor3f(0.419608, 0.556863, 0.137255);
    glTranslatef(0.8f,0.4f,1.2f);
    //glScalef(0.3,0.3,0.3);
    glutSolidSphere(0.5,20,10);

    glPushMatrix();//pupila drept
    glColor3f(0.0f,0.0f,0.0f);
    glTranslatef(0.0f,0.0f,0.5f);
    //glScalef(0.1,0.1,0.1);
    glutSolidSphere(0.3,20,10);

    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();

////////////////////////////////////
///PICIOARE
////////////////////////////////////

    glPushMatrix(); //picior stang
    glColor3f(1.0f, 0.5f, 0.0f);
    glTranslatef(-1.0f,-3.0f,0.0f);
    glRotatef(90,1,0,0);
    //glRotatef(-x,1,0,0);
    glScalef(1,1,1);
    glutSolidCylinder(0.5,1,20,10);

    glPushMatrix();//labuta stang
    glColor3f(1.0 , 0.5, 0.0);
    glTranslated(0.0f, 0.5f,1.0f);
    glRotatef(90,0,0,1);
    //glRotatef(-x,0,1,0);
    glScalef(2,1,0.5);
    glutSolidSphere(0.7,8,10);

    glPopMatrix();
    glPopMatrix();

    glPushMatrix(); //picior drept
    glColor3f(1.0f, 0.5f, 0.0f);
    glTranslatef(1.0f,-3.0f,0.0f);
    glRotatef(90,1,0,0);
    glScalef(1,1,1);
    glutSolidCylinder(0.5,1,20,10);


    glPushMatrix();//labuta drept
    glColor3f(1.0 , 0.5, 0.0);
    glTranslated(0.0f, 0.5f,1.0f);
    glRotatef(90,0,0,1);
    glScalef(2,1,0.5);
    glutSolidSphere(0.7,8,10);

    glPopMatrix();
    glPopMatrix();

///////////////////////////////////////
///ARIPI
///////////////////////////////////////

    glPushMatrix(); //aripa stanga
     glColor3f(1.0 , 1.0, 0.0);
    glTranslatef(-2.8f,0.0f,-0.0f);
    glRotatef(-90,1,0,0);
    glRotatef(y,0,1,0);
    glScalef(0.5,2,1);
    glutSolidSphere(1.3,16,10);
    glPopMatrix();

    glPushMatrix(); //aripa dreapta
     glColor3f(1.0 , 1.0, 0.0);
    glTranslatef(2.8f,0.0f,0.0f);
    glRotatef(-90,1,0,0);
    glRotatef(-y,0,1,0);
    glScalef(0.5,2,1);
    glutSolidSphere(1.3,16,10);
    glPopMatrix();

//////////////////////////////////////
///COADA
//////////////////////////////////////

    glPushMatrix(); //coada
    glColor3f(1.0 , 1.0, 0.0);
    glTranslatef(0.0f,1.8f,-3.0f);
    glRotatef(130,0,0,1);
    glScalef(1,1,0.3);
    glutSolidSphere(1.3,8,10);
    glPopMatrix();

    glPopMatrix();

    x += 0.04f; x=fmod(x,30.0f);
    if(y == 0.0f){
        y+=2.0f;
    }
    if(y <= 80.0f){
        y -= 2.0f;
    }
    while(bow != -5.0f && k==1){
                bow -= 0.5f;
                break;
    }
    while(bow != 60.0f && k==0){
                bow += 0.5f;
                break;
            }
    while(lean != -60.0f && l==-1){
                lean -= 0.5f;
                break;
    }
    while(lean != 60.0f && l==1){
                lean += 0.5f;
                break;
            }
    if(l=0){
            lean = 0.0f;
    }
    axisRot += 0.04f; axisRot=fmod(axisRot, 360.0f);


    glutSwapBuffers();
}
static void
idle(void)
{
    glutPostRedisplay();
}

void keyboard(unsigned char key , int x , int y) {
    switch (key) {
        case 27: //ESC
            exit(0);
            break;
        case 32:
            angle += 90.0f;
            break;
        case 43:
            zoom += 1.0f;
            break;
        case 45:
            zoom -= 1.0f;
            break;
        case 's':
            k=1;
            break;
        case 'w':
            k=0;
            break;
        case 'd':
            l=1;
            break;
        case 'a':
            l=-1;
            break;
        case 'q':
            l=0;
            break;
    }
}

int main(int argc,char *argv[])
{
    int width=840;
    int height=680;
    glutInit(&argc,argv);
    glutInitWindowSize(width,height);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB|GLUT_DOUBLE|GLUT_DEPTH);
    glutCreateWindow("");
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(keyboard);
    initGL(width,height);
    glutMainLoop();
    return EXIT_SUCCESS;

}
